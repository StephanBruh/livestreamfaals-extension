window.onload = function() {
  var metaTags = document.getElementsByTagName('meta');

  var loadButton = setInterval(function() {
    if (document.querySelector('.clips-editor-slider-background.tw-mg-y-4')) {
      if (!document.querySelector('.quickLivestreamFaal')) {
        document.querySelector('.tw-align-items-center.tw-flex.tw-justify-content-between.tw-pd-t-1').insertAdjacentHTML('afterend','<p class="quickLivestreamFaal" style="cursor: pointer; padding: 5px; font-size: 25px; background-color:#6441a5; text-align: center; color: white; margin-top: 10px; border-radius: 5px;">Posten op r/LivestreamFaals</p>');
      } else {
        clearInterval(loadButton);
        document.querySelector('.quickLivestreamFaal').onclick = () => {
        document.querySelector('.tw-align-items-center.tw-align-middle.tw-border-bottom-left-radius-large.tw-border-bottom-right-radius-large.tw-border-top-left-radius-large.tw-border-top-right-radius-large.tw-core-button.tw-core-button--large.tw-core-button--primary.tw-inline-flex.tw-interactive.tw-justify-content-center.tw-overflow-hidden.tw-relative').click();
          if (!document.querySelector('.tw-block.tw-border-bottom-left-radius-large.tw-border-bottom-right-radius-large.tw-border-top-left-radius-large.tw-border-top-right-radius-large.tw-font-size-5.tw-full-width.tw-input.tw-input--large.tw-pd-l-1.tw-pd-r-1.tw-pd-y-05').getAttribute('value') == '') {
            for (var i = 0; i < metaTags.length; i++) {
              if (metaTags[i].getAttribute('property') == 'og:url') {
                var postURL = `https://reddit.com/r/LivestreamFaals/submit?url=${metaTags[i].getAttribute('content').slice(0,-5)}&title=${document.querySelector('.tw-block.tw-border-bottom-left-radius-large.tw-border-bottom-right-radius-large.tw-border-top-left-radius-large.tw-border-top-right-radius-large.tw-font-size-5.tw-full-width.tw-input.tw-input--large.tw-pd-l-1.tw-pd-r-1.tw-pd-y-05').getAttribute('value')}&ezyKarma=true`;
                window.open(postURL);
                break;
              }
            }
          }
        }
      }
    }
  }, 500);
}
